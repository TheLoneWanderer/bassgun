﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TestCoroutine : MonoBehaviour
{
    public Rigidbody rb;
    public Vector3 startPosition;
    public Vector3 targetDestination;

    public Quaternion startRotation;
    public Quaternion targetRotation;


    // Start is called before the first frame update
    void Start()
    {
        rb = GetComponent<Rigidbody>();
        startPosition = transform.position;
        targetDestination = new Vector3(startPosition.x, startPosition.y, startPosition.z + 20.0f);

        StartCoroutine(MoveToPointCoroutine());

        //StartCoroutine(Rotate90DegreesSlerp());

        startRotation = transform.rotation;
        targetRotation = startRotation * Quaternion.Euler(new Vector3(90.0f, 0f, 0f));
    }

    // Update is called once per frame
    void Update()
    {
        Rotate90DegreesRotateTowards();
    }

    private IEnumerator MoveToPointCoroutine()
    {
        for (int i=0; i<=10; ++i)
        {
            transform.position = Vector3.Lerp(startPosition, targetDestination, i * 0.1f);
            yield return new WaitForSeconds(0.2f);
        }
    }

    private IEnumerator Rotate90DegreesSlerp()
    {
        var startingRotation = transform.rotation;
        var targetRotation = transform.rotation * Quaternion.Euler(new Vector3(90f, 0f, 0f));

        for (int i=0; i<=10; ++i)
        {
            transform.rotation = Quaternion.Slerp(startingRotation, targetRotation, i * 0.1f);
            yield return new WaitForSeconds(0.2f);
        }
    }
    
    private void Rotate90DegreesRotateTowards()
    {
        transform.rotation = Quaternion.RotateTowards(transform.rotation, targetRotation, 10 * Time.deltaTime);
    }
}
