﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;

namespace Assets.Code.Combat.SpawningObjects
{
    public class ObjectPooler : MonoBehaviour
    {
        [SerializeField]
        private List<PoolType> poolTypes;
        private Dictionary<string, Queue<GameObject>> prefabPools;

        void Awake()
        {
            prefabPools = new Dictionary<string, Queue<GameObject>>();
            InitializePrefabPools();
        }

        public GameObject PullObject(string sourcePoolKey)
        {
            var poolQueue = prefabPools[sourcePoolKey];
            if(poolQueue.Count == 0)
            {
                return null;
            }

            return poolQueue.Dequeue();
        }

        public void PutBackObject(string destinationPoolKey, GameObject gameObject)
        {
            ResetObject(gameObject);
            prefabPools[destinationPoolKey].Enqueue(gameObject);
        }

        public int GetPoolPopulationSize(string poolKey)
        {
            return prefabPools[poolKey].Count;
        }

        private void InitializePrefabPools()
        {
            foreach (var poolType in poolTypes)
            {
                var poolKey = poolType.poolKey;
                prefabPools[poolKey] = new Queue<GameObject>();

                for (int i=0; i<poolType.poolSize; i++)
                {
                    GameObject prefabInstance = Instantiate(poolType.prefab);
                    prefabInstance.SetActive(false);

                    prefabPools[poolKey].Enqueue(prefabInstance);
                }
            }
        }

        /// <summary>
        /// Clear the object's state before storing it back
        /// </summary>
        /// <param name="gameObject"></param>
        private void ResetObject(GameObject gameObject)
        {
            gameObject.SetActive(false);
            gameObject.transform.position = transform.position;
        }

        [Serializable]
        public class PoolType
        {
            public string poolKey;
            public int poolSize;
            public GameObject prefab;
        }
    }
}
