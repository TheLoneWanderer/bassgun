﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;

namespace Assets.Code.Combat.SpawningObjects.ObjectPooling
{
    public class PoolableObjectData : MonoBehaviour, IPoolable
    {
        [SerializeField]
        private string poolKey;

        public string GetPoolKey()
        {
            return poolKey;
        }
    }
}
