﻿using Assets.Code.Helpers.Constants;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Assets.Code.Combat.SpawningObjects
{
    public class TimedSpawner : MonoBehaviour
    {
        public List<Transform> spawnersTransforms;

        public string objectsPoolKey;
        private ObjectPooler objectPooler;
        private int objectsPoolInitialSize;

        public int populationLimit;
        public int spawningCooldown;

        void Start()
        {
            objectPooler = GetComponent<ObjectPooler>();
            objectsPoolInitialSize = objectPooler.GetPoolPopulationSize(objectsPoolKey);

            StartCoroutine("SpawnEnemyLoopedCoroutine");
        }

        IEnumerator SpawnEnemyLoopedCoroutine()
        {
            while(true)
            {
                var currentPopulation = objectsPoolInitialSize - objectPooler.GetPoolPopulationSize(objectsPoolKey);
                if (populationLimit > currentPopulation)
                {
                    var pulledObject = objectPooler.PullObject(objectsPoolKey);
                    if (pulledObject != null)
                    {
                        SetObjectDefaults(pulledObject);
                    }
                }            

                yield return new WaitForSeconds(spawningCooldown);
            }
        }

        void SetObjectDefaults(GameObject gameObject)
        {
            var randomSpawnerIndex = Random.Range(0, spawnersTransforms.Count);
            var randomStartingPosition = spawnersTransforms[randomSpawnerIndex].position;

            gameObject.transform.position = randomStartingPosition;
            gameObject.SetActive(true);
        }
    }
}
