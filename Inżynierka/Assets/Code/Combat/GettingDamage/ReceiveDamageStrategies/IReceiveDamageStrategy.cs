﻿using Assets.Code.Helpers.DTO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine.UI;

namespace Assets.Code.ReceiveDamageStrategies
{
    public interface IReceiveDamageStrategy
    {
        void ReceiveDamage(DamageCharacterEntry characterEntry);
    }
}
