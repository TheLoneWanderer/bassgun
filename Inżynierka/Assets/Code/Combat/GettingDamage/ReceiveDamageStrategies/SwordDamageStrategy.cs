﻿using Assets.Code.Helpers.DTO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine.UI;

namespace Assets.Code.ReceiveDamageStrategies
{
    class TwoHandedSwordDamageStrategy : IReceiveDamageStrategy
    {
        public void ReceiveDamage(DamageCharacterEntry characterEntry)
        {
            characterEntry.healthSlider.value -= 40;
        }
    }
}
