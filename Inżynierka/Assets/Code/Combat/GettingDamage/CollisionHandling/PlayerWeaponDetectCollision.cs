﻿using Assets.Code.Helpers.Constants;
using Assets.Code.ReceiveDamageStrategies;
using Assets.Code.Controllers.Enemy;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerWeaponDetectCollision : MonoBehaviour
{
    private void OnTriggerEnter(Collider other)
    {
        if(other.gameObject.tag == Consts.HittableEnemyTag)
        {
            var enemyControllerScript = other.gameObject.GetComponent<EnemyController>();
            enemyControllerScript.TakeDamage(new TwoHandedSwordDamageStrategy());
        }
    }
}
