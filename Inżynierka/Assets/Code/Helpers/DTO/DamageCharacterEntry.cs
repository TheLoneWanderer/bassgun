﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;
using UnityEngine.UI;

namespace Assets.Code.Helpers.DTO
{
    /// <summary>
    /// Used by the damage dealing strategy to access only the necessary character variables
    /// </summary>
    public class DamageCharacterEntry
    {
        public Slider healthSlider { get; set; }
    }
}
