﻿using UnityEngine;
using System.Collections;

namespace Assets.Code.Controllers.Character{

	public class IKHandsFREE : MonoBehaviour{
		private Animator animator;
		private CharacterWeaponController rpgCharacterWeaponController;
		public Transform leftHandObj;
		public Transform attachLeft;
		[Range(0, 1)] public float leftHandPositionWeight;
		[Range(0, 1)] public float leftHandRotationWeight;
		Transform blendToTransform;
		
		void Awake() {
			animator = GetComponent<Animator>();
			rpgCharacterWeaponController = GetComponent<CharacterWeaponController>();
		}
		
		void OnAnimatorIK(int layerIndex){
			if(leftHandObj != null){
				animator.SetIKPositionWeight(AvatarIKGoal.LeftHand, leftHandPositionWeight);
				animator.SetIKRotationWeight(AvatarIKGoal.LeftHand, leftHandRotationWeight);
				animator.SetIKPosition(AvatarIKGoal.LeftHand, attachLeft.position);                    
				animator.SetIKRotation(AvatarIKGoal.LeftHand, attachLeft.rotation);
			}
		}

		// IK Weight 0 - UNARMED
		// IK Weight 1 - TWOHANDSWORD
		public void BlendIK(bool foldHands, float delay, float timeToBlend, Weapon equippedWeaponType)
		{
			StopCoroutine("BlendIKCoroutine");

			object[] parameters = { foldHands, delay, timeToBlend, equippedWeaponType };
			StartCoroutine("BlendIKCoroutine", parameters);
		}

		private IEnumerator BlendIKCoroutine(object[] parameters)
		{
			bool foldHands = (bool)parameters[0];
			float delay = (float)parameters[1];
			float timeToBlend = (float)parameters[2];
			Weapon equippedWeaponType = (Weapon)parameters[3];

			//If not using 2 handed weapon, quit function.
			if (equippedWeaponType == Weapon.UNARMED)
			{
				yield break;
			}
			GetCurrentWeaponAttachPoint(equippedWeaponType);

			yield return new WaitForSeconds(delay);
			float t = 0f;
			float blendTo = 0;
			float blendFrom = 0;
			if (foldHands)
			{
				blendTo = 1;
			}
			else
			{
				blendFrom = 1;
			}
			while (t < 1)
			{
				t += Time.deltaTime / timeToBlend;
				attachLeft = blendToTransform;
				leftHandPositionWeight = Mathf.Lerp(blendFrom, blendTo, t);
				leftHandRotationWeight = Mathf.Lerp(blendFrom, blendTo, t);
				yield return null;
			}
			yield break;
		}

		void GetCurrentWeaponAttachPoint(Weapon equippedWeaponType)
		{
			if(equippedWeaponType != Weapon.UNARMED){
				blendToTransform = rpgCharacterWeaponController.equippedWeapon.transform.GetChild(0).transform;
			}
		}
	}
}