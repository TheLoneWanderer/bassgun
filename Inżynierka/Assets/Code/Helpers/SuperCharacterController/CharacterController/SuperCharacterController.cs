﻿// External release version 2.0.0
using UnityEngine;
using System;
using System.Linq;
using System.Collections.Generic;

/// <summary>
/// Custom character controller, to be used by attaching the component to an object
/// and writing scripts attached to the same object that recieve the "SuperUpdate" message
/// </summary>
public partial class SuperCharacterController : MonoBehaviour
{
    [SerializeField]
    QueryTriggerInteraction triggerInteraction;

    [SerializeField]
    bool clampToMovingGround;

    CollisionSphere[] characterSpheres =
        new CollisionSphere[3] {
            new CollisionSphere(0.6f, true, false),
            new CollisionSphere(1.2f, false, false),
            new CollisionSphere(1.8f, false, true),
        };

    float sphereRadius = 0.6f;

    private LayerMask WalkableLayer;

    [SerializeField]
    Collider ownCollider;

    public float deltaTime { get; private set; }
    public SuperGround currentGround { get; private set; }
    public CollisionSphere feet { get; private set; }
    public CollisionSphere head { get; private set; }

    /// <summary>
    /// Total height of the controller from the bottom of the feet to the top of the head
    /// </summary>
    public float height { get { return Vector3.Distance(SpherePosition(head), SpherePosition(feet)) + sphereRadius * 2; } }

    public Vector3 up { get { return transform.up; } }
    public Vector3 down { get { return -transform.up; } }
    public List<SuperCollision> collisionData { get; private set; }
    public Transform currentlyClampedTo { get; set; }
    public float heightScale { get; set; }
    public float radiusScale { get; set; }

    private Vector3 initialPosition;
    private Vector3 lastGroundPosition;
    private bool canClamp = true;
    private bool canWalkSlopes = true;

    private List<Collider> ignoredColliders;
    private List<IgnoredCollider> ignoredColliderStack;

    private const float Tolerance = 0.05f;
    private const float TinyTolerance = 0.01f;
    private const int MaxPushbackIterations = 2;

    private static SuperCollisionType defaultCollisionType;

    void Awake()
    {
        collisionData = new List<SuperCollision>();

        ignoredColliders = new List<Collider>();
        ignoredColliderStack = new List<IgnoredCollider>();

        currentlyClampedTo = null;

        heightScale = 1.0f;

        if (ownCollider)
            IgnoreCollider(ownCollider);

        foreach (var sphere in characterSpheres)
        {
            if (sphere.isFeet)
                feet = sphere;

            if (sphere.isHead)
                head = sphere;
        }

        if (feet == null)
            Debug.LogError("[SuperCharacterController] Feet not found on controller");

        if (head == null)
            Debug.LogError("[SuperCharacterController] Head not found on controller");

        if (defaultCollisionType == null)
            defaultCollisionType = new GameObject("DefaultSuperCollisionType", typeof(SuperCollisionType)).GetComponent<SuperCollisionType>();

        WalkableLayer = 1;
        currentGround = new SuperGround(WalkableLayer, this, triggerInteraction);
    }

    void Update()
    {
        deltaTime = Time.deltaTime;

        // Check if we are clamped to an object implicity or explicity
        bool isClamping = canClamp || currentlyClampedTo != null;
        Transform clampedTo = currentlyClampedTo == null ? currentGround.groundTransform : currentlyClampedTo;

        if (clampToMovingGround && isClamping && (clampedTo != null) && (clampedTo.position - lastGroundPosition) != Vector3.zero)
            transform.position += clampedTo.position - lastGroundPosition;

        initialPosition = transform.position;

        ProbeGround(1);
        gameObject.SendMessage("SuperUpdate", SendMessageOptions.DontRequireReceiver);

        collisionData.Clear();
        RecursivePushback(0, MaxPushbackIterations);
        ProbeGround(2);

        if (canWalkSlopes)
            SlopeLimit();

        ProbeGround(3);

        if (canClamp)
            ClampToGround();

        isClamping = canClamp || currentlyClampedTo != null;
        clampedTo = currentlyClampedTo == null ? currentGround.groundTransform : currentlyClampedTo;

        if (isClamping)
            lastGroundPosition = clampedTo.position;
    }

    void ProbeGround(int iter)
    {
        PushIgnoredColliders();
        currentGround.ProbeGround(SpherePosition(feet), iter);
        PopIgnoredColliders();
    }

    /// <summary>
    /// Check if any of the CollisionSpheres are colliding with any walkable objects in the world.
    /// If they are, apply a proper pushback and retrieve the collision data
    /// </summary>
    void RecursivePushback(int depth, int maxDepth)
    {
        PushIgnoredColliders();
        bool contact = false;

        foreach (var characterPartSphere in characterSpheres)
        {
            var overlappingColliders = Physics.OverlapSphere(SpherePosition(characterPartSphere), sphereRadius, WalkableLayer, triggerInteraction);

            var spherePosDebug = SpherePosition(characterPartSphere);
            Debug.DrawLine(spherePosDebug, spherePosDebug + new Vector3(sphereRadius, 0, 0));
            Debug.DrawLine(spherePosDebug, spherePosDebug + new Vector3(0, sphereRadius, 0));

            foreach (Collider overlappingCollider in overlappingColliders)
            {
                Vector3 characterSpherePosition = SpherePosition(characterPartSphere);
                bool contactPointSuccess = SuperCollider.ClosestPointOnSurface(overlappingCollider, characterSpherePosition, sphereRadius, out Vector3 contactPoint);

                if (!contactPointSuccess)
                {
                    continue;
                }

                Vector3 collisionDepthVector = contactPoint - characterSpherePosition;
                if (collisionDepthVector != Vector3.zero)
                {
                    // Cache the collider's layer so that we can cast against it
                    int colliderLayer = overlappingCollider.gameObject.layer;

                    // Check which side of the normal we are on
                    Debug.DrawLine(characterSpherePosition, characterSpherePosition + collisionDepthVector, Color.red);
                    Debug.DrawLine(characterSpherePosition + collisionDepthVector, characterSpherePosition + collisionDepthVector - Vector3.right * 0.2f, Color.green);
                    Debug.DrawLine(characterSpherePosition + collisionDepthVector, characterSpherePosition + collisionDepthVector + Vector3.right * 0.2f, Color.cyan);

                    bool facingNormal = Physics.SphereCast(new Ray(characterSpherePosition, collisionDepthVector.normalized), TinyTolerance, collisionDepthVector.magnitude + TinyTolerance, WalkableLayer);

                    // Orient and scale our vector based on which side of the normal we are situated
                    if (facingNormal)
                    {
                        if (Vector3.Distance(characterSpherePosition, contactPoint) < sphereRadius)
                        {
                            collisionDepthVector = collisionDepthVector.normalized * (sphereRadius - collisionDepthVector.magnitude) * -1;
                        }
                        else
                        {
                            // A previously resolved collision has had a side effect that moved us outside this collider
                            continue;
                        }
                    }
                    else
                    {
                        collisionDepthVector = collisionDepthVector.normalized * (sphereRadius + collisionDepthVector.magnitude);
                    }

                    contact = true;
                    transform.position += collisionDepthVector;

                    // Retrieve the surface normal of the collided point
                    Physics.SphereCast(new Ray(characterSpherePosition + collisionDepthVector, contactPoint - (characterSpherePosition + collisionDepthVector)), TinyTolerance, out RaycastHit normalHit, WalkableLayer);

                    SuperCollisionType superColType = overlappingCollider.gameObject.GetComponent<SuperCollisionType>();
                    if (superColType == null)
                        superColType = defaultCollisionType;

                    // Our collision affected the collider; add it to the collision data
                    var collision = new SuperCollision()
                    {
                        collisionSphere = characterPartSphere,
                        superCollisionType = superColType,
                        gameObject = overlappingCollider.gameObject,
                        point = contactPoint,
                        normal = normalHit.normal
                    };

                    collisionData.Add(collision);
                }
            }
        }

        PopIgnoredColliders();

        if (depth < maxDepth && contact)
        {
            RecursivePushback(depth + 1, maxDepth);
        }
    }

    /// <summary>
    /// Prevents the player from walking up slopes of a larger angle than the object's SlopeLimit.
    /// </summary>
    /// <returns>True if the controller attemped to ascend a too steep slope and had their movement limited</returns>
    bool SlopeLimit()
    {
        Vector3 normal = currentGround.PrimaryNormal();
        float groundAngle = Vector3.Angle(normal, up);

        if (groundAngle > currentGround.superCollisionType.SlopeLimit)
        {
            Vector3 absoluteMoveDirection = Math3d.ProjectVectorOnPlane(normal, transform.position - initialPosition);

            // Retrieve a vector pointing down the slope
            Vector3 acrossSlope = Vector3.Cross(normal, down);
            Vector3 downTheSlope = Vector3.Cross(acrossSlope, normal);

            float angle = Vector3.Angle(absoluteMoveDirection, downTheSlope);

            if (angle <= 90.0f)
                return false;

            // Calculate where to place the controller on the slope, or at the bottom, based on the desired movement distance
            Vector3 resolvedPosition = Math3d.ProjectPointOnLine(initialPosition, acrossSlope, transform.position);
            Vector3 direction = Math3d.ProjectVectorOnPlane(normal, resolvedPosition - transform.position);

            // Check if our path to our resolved position is blocked by any colliders
            if (Physics.CapsuleCast(SpherePosition(feet), SpherePosition(head), sphereRadius, direction.normalized, out RaycastHit hit, direction.magnitude, WalkableLayer, triggerInteraction))
            {
                transform.position += downTheSlope.normalized * hit.distance;
            }
            else
            {
                transform.position += direction;
            }

            return true;
        }

        return false;
    }

    void ClampToGround()
    {
        float d = currentGround.Distance();
        transform.position -= up * d;
    }

    public void EnableClamping()
    {
        canClamp = true;
    }

    public void DisableClamping()
    {
        canClamp = false;
    }

    public void EnableSlopeLimit()
    {
        canWalkSlopes = true;
    }

    public void DisableSlopeLimit()
    {
        canWalkSlopes = false;
    }

    public bool IsClamping()
    {
        return canClamp;
    }

    protected struct IgnoredCollider
    {
        public Collider collider;
        public int layer;

        public IgnoredCollider(Collider collider, int layer)
        {
            this.collider = collider;
            this.layer = layer;
        }
    }

    private void PushIgnoredColliders()
    {
        ignoredColliderStack.Clear();

        for (int i = 0; i < ignoredColliders.Count; i++)
        {
            Collider ignoredCollider = ignoredColliders[i];
            ignoredCollider.gameObject.layer = WalkableLayer;

            // The WalkableLayer (1) is set to ignore the whole model sphere collider while checking for collisions inside this script,
            // otherwise it would detect that the player's feet/torso/head colliders are inside the big one and cause continuous jumping 
            // It would be detected as colliding with itself in simple words.
            // Set back to the Default layer (0) later to let it be detected by other SuperCharacters
            ignoredColliderStack.Add(new IgnoredCollider(ignoredCollider, 0));
        }
    }

    private void PopIgnoredColliders()
    {
        for (int i = 0; i < ignoredColliderStack.Count; i++)
        {
            IgnoredCollider ic = ignoredColliderStack[i];
            ic.collider.gameObject.layer = ic.layer;
        }

        ignoredColliderStack.Clear();
    }

    public Vector3 SpherePosition(CollisionSphere sphere)
    {
        if (sphere.isFeet)
            return transform.position + (sphere.offset * up);
        else
            return transform.position + (sphere.offset * up * heightScale);
    }

    public bool PointBelowHead(Vector3 point)
    {
        return Vector3.Angle(point - SpherePosition(head), up) > 89.0f;
    }

    public bool PointAboveFeet(Vector3 point)
    {
        return Vector3.Angle(point - SpherePosition(feet), down) > 89.0f;
    }

    public void IgnoreCollider(Collider col)
    {
        ignoredColliders.Add(col);
    }

    public void RemoveIgnoredCollider(Collider col)
    {
        ignoredColliders.Remove(col);
    }

    public void ClearIgnoredColliders()
    {
        ignoredColliders.Clear();
    }
}

[Serializable]
public class CollisionSphere
{
    public float offset;
    public bool isFeet;
    public bool isHead;

    public CollisionSphere(float offset, bool isFeet, bool isHead)
    {
        this.offset = offset;
        this.isFeet = isFeet;
        this.isHead = isHead;
    }
}

/// <summary>
/// Describes the Transform of the object we are standing on as well as its CollisionType, as well
/// as how far the ground is below us and what angle it is in relation to the controller.
/// </summary>
[SerializeField]
public struct Ground
{
    public RaycastHit hit { get; set; }
    public RaycastHit nearHit { get; set; }
    public RaycastHit farHit { get; set; }
    public RaycastHit secondaryHit { get; set; }
    public SuperCollisionType collisionType { get; set; }
    public Transform transform { get; set; }

    public Ground(RaycastHit hit, RaycastHit nearHit, RaycastHit farHit, RaycastHit secondaryHit, SuperCollisionType superCollisionType, Transform hitTransform)
    {
        this.hit = hit;
        this.nearHit = nearHit;
        this.farHit = farHit;
        this.secondaryHit = secondaryHit;
        this.collisionType = superCollisionType;
        this.transform = hitTransform;
    }
}

public struct SuperCollision
{
    public CollisionSphere collisionSphere;
    public SuperCollisionType superCollisionType;
    public GameObject gameObject;
    public Vector3 point;
    public Vector3 normal;
}
