﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;

public partial class SuperCharacterController
{
    public class SuperGround
    {
        public SuperGround(LayerMask walkableLayer, SuperCharacterController characterController, QueryTriggerInteraction triggerInteraction)
        {
            this.walkableLayer = walkableLayer;
            this.characterController = characterController;
            this.triggerInteraction = triggerInteraction;
        }

        private class GroundHit
        {
            public Vector3 Point { get; private set; }
            public Vector3 Normal { get; private set; }
            public float Distance { get; private set; }

            public GroundHit(Vector3 point, Vector3 normal, float distance)
            {
                this.Point = point;
                this.Normal = normal;
                this.Distance = distance;
            }
        }

        private LayerMask walkableLayer;
        private SuperCharacterController characterController;
        private QueryTriggerInteraction triggerInteraction;

        private GroundHit primaryGround;
        private GroundHit nearGround;
        private GroundHit farGround;
        private GroundHit stepGround;
        private GroundHit flushGround;

        public SuperCollisionType superCollisionType { get; private set; }
        public Transform groundTransform { get; private set; }

        private const float groundingUpperBoundAngle = 60.0f;
        private const float groundingMaxPercentFromCenter = 0.85f;
        private const float groundingMinPercentFromcenter = 0.50f;

        /// <summary>
        /// Scan the surface below us for ground. Follow up the initial scan with subsequent scans
        /// designed to test what kind of surface we are standing above and handle different edge cases
        /// </summary>
        /// <param name="origin">Center of the sphere for the initial SphereCast</param>
        /// <param name="iter">Debug tool to print out which ProbeGround iteration is being run (3 are run each frame for the characterController)</param>
        public void ProbeGround(Vector3 origin, int iter)
        {
            ResetGrounds();

            Vector3 up = characterController.up;
            Vector3 down = -up;

            Vector3 castOrigin = origin + (up * Tolerance);
            
            // Reduce our radius by Tolerance squared to avoid failing the SphereCast due to clipping with walls
            float smallerRadius = characterController.sphereRadius - (Tolerance * Tolerance);

            if (Physics.SphereCast(castOrigin, smallerRadius, down, out RaycastHit rayHitInfo, Mathf.Infinity, walkableLayer))
            {
                var superColType = rayHitInfo.collider.gameObject.GetComponent<SuperCollisionType>();
                if (superColType == null)
                {
                    superColType = defaultCollisionType;
                }

                superCollisionType = superColType;
                groundTransform = rayHitInfo.transform;

                // By reducing the initial SphereCast's radius by Tolerance, our casted sphere no longer fits with
                // our characterController's shape. Reconstruct the sphere cast with the proper radius-
                SimulateSphereCast(rayHitInfo.normal, out rayHitInfo);

                primaryGround = new GroundHit(rayHitInfo.point, rayHitInfo.normal, rayHitInfo.distance);

                // If we are standing on a perfectly flat surface, we cannot be either on an edge,
                // On a slope or stepping off a ledge
                if (Vector3.Distance(Math3d.ProjectPointOnPlane(up, characterController.transform.position, rayHitInfo.point), characterController.transform.position) < TinyTolerance)
                {
                    return;
                }

                // As we are standing on an edge, we need to retrieve the normals of the two
                // faces on either side of the edge and store them in nearHit and farHit
                Vector3 toCenter = Math3d.ProjectVectorOnPlane(up, (characterController.transform.position - rayHitInfo.point).normalized * TinyTolerance);

                Vector3 awayFromCenter = Quaternion.AngleAxis(-80.0f, Vector3.Cross(toCenter, up)) * -toCenter;

                Vector3 nearPoint = rayHitInfo.point + toCenter + (up * TinyTolerance);
                Vector3 farPoint = rayHitInfo.point + (awayFromCenter * 3);

                Physics.Raycast(nearPoint, down, out RaycastHit nearHit, Mathf.Infinity, walkableLayer, triggerInteraction);
                Physics.Raycast(farPoint, down, out RaycastHit farHit, Mathf.Infinity, walkableLayer, triggerInteraction);

                nearGround = new GroundHit(nearHit.point, nearHit.normal, nearHit.distance);
                farGround = new GroundHit(farHit.point, farHit.normal, farHit.distance);


                // https://homeguides.sfgate.com/keep-heavy-paintings-flush-against-wall-52888.html
                // "flush against the wall" wyjaśnione

                // If we are currently standing on ground that should be counted as a wall,
                // we are likely flush against it on the ground. Retrieve what we are standing on
                if (Vector3.Angle(rayHitInfo.normal, up) > superColType.StandAngle)
                {
                    // Retrieve a vector pointing down the slope
                    Vector3 acrossSlope = Vector3.Cross(rayHitInfo.normal, down);
                    Vector3 downTheSlope = Vector3.Cross(acrossSlope, rayHitInfo.normal);

                    Vector3 flushOrigin = rayHitInfo.point + rayHitInfo.normal * TinyTolerance;

                    if (Physics.Raycast(flushOrigin, downTheSlope, out RaycastHit flushHit, Mathf.Infinity, walkableLayer, triggerInteraction))
                    {

                        if (SimulateSphereCast(flushHit.normal, out RaycastHit sphereCastHit))
                        {
                            flushGround = new GroundHit(sphereCastHit.point, sphereCastHit.normal, sphereCastHit.distance);
                        }
                        else
                        {
                            // Uh oh
                        }
                    }
                }

                // If we are currently standing on a ledge then the face nearest the center of the
                // characterController should be steep enough to be counted as a wall. Retrieve the ground
                // it is connected to at it's base, if there exists any
                if (Vector3.Angle(nearHit.normal, up) > superColType.StandAngle || nearHit.distance > Tolerance)
                {
                    var col = nearHit.collider.gameObject.GetComponent<SuperCollisionType>();
                    if (col == null)
                    {
                        col = defaultCollisionType;
                    }

                    // We contacted the wall of the ledge, rather than the landing. Raycast down
                    // the wall to retrieve the proper landing
                    if (Vector3.Angle(nearHit.normal, up) > col.StandAngle)
                    {
                        // Retrieve a vector pointing down the slope
                        Vector3 r = Vector3.Cross(nearHit.normal, down);
                        Vector3 v = Vector3.Cross(r, nearHit.normal);

                        if (Physics.Raycast(nearPoint, v, out RaycastHit stepHit, Mathf.Infinity, walkableLayer, triggerInteraction))
                        {
                            stepGround = new GroundHit(stepHit.point, stepHit.normal, stepHit.distance);
                        }
                    }
                    else
                    {
                        stepGround = new GroundHit(nearHit.point, nearHit.normal, nearHit.distance);
                    }
                }
            }
            // If the initial SphereCast fails, likely due to the characterController clipping a wall,
            // fallback to a raycast simulated to SphereCast data
            else if (Physics.Raycast(castOrigin, down, out rayHitInfo, Mathf.Infinity, walkableLayer, triggerInteraction))
            {
                var superColType = rayHitInfo.collider.gameObject.GetComponent<SuperCollisionType>();

                if (superColType == null)
                {
                    superColType = defaultCollisionType;
                }

                superCollisionType = superColType;
                groundTransform = rayHitInfo.transform;

                if (SimulateSphereCast(rayHitInfo.normal, out RaycastHit sphereCastHit))
                {
                    primaryGround = new GroundHit(sphereCastHit.point, sphereCastHit.normal, sphereCastHit.distance);
                }
                else
                {
                    primaryGround = new GroundHit(rayHitInfo.point, rayHitInfo.normal, rayHitInfo.distance);
                }
            }
            else
            {
                Debug.LogError("[SuperCharacterComponent]: No ground was found below the player; player has escaped level");
            }
        }

        private void ResetGrounds()
        {
            primaryGround = null;
            nearGround = null;
            farGround = null;
            flushGround = null;
            stepGround = null;
        }

        public bool IsGrounded(bool currentlyGrounded, float distance)
        {
            return IsGrounded(currentlyGrounded, distance, out _);
        }

        public bool IsGrounded(bool currentlyGrounded, float distance, out Vector3 groundNormal)
        {
            groundNormal = Vector3.zero;

            if (primaryGround == null || primaryGround.Distance > distance)
            {
                return false;
            }

            // Check if we are flush against a wall
            if (farGround != null && Vector3.Angle(farGround.Normal, characterController.up) > superCollisionType.StandAngle)
            {
                if (flushGround != null && Vector3.Angle(flushGround.Normal, characterController.up) < superCollisionType.StandAngle && flushGround.Distance < distance)
                {
                    groundNormal = flushGround.Normal;
                    return true;
                }

                return false;
            }

            // Check if we are at the edge of a ledge, or on a high angle slope
            if (farGround != null && !OnSteadyGround(farGround.Normal, primaryGround.Point))
            {
                // Check if we are walking onto steadier ground
                if (nearGround != null && nearGround.Distance < distance && Vector3.Angle(nearGround.Normal, characterController.up) < superCollisionType.StandAngle && !OnSteadyGround(nearGround.Normal, nearGround.Point))
                {
                    groundNormal = nearGround.Normal;
                    return true;
                }

                // Check if we are on a step or stair
                if (stepGround != null && stepGround.Distance < distance && Vector3.Angle(stepGround.Normal, characterController.up) < superCollisionType.StandAngle)
                {
                    groundNormal = stepGround.Normal;
                    return true;
                }

                return false;
            }


            if (farGround != null)
            {
                groundNormal = farGround.Normal;
            }
            else
            {
                groundNormal = primaryGround.Normal;
            }

            return true;
        }

        /// <summary>
        /// To help the characterController smoothly "fall" off surfaces and not hang on the edge of ledges,
        /// check that the ground below us is "steady", or that the characterController is not standing
        /// on too extreme of a ledge
        /// </summary>
        /// <param name="normal">Normal of the surface to test against</param>
        /// <param name="point">Point of contact with the surface</param>
        /// <returns>True if the ground is steady</returns>
        private bool OnSteadyGround(Vector3 normal, Vector3 point)
        {
            float angle = Vector3.Angle(normal, characterController.up);

            float angleRatio = angle / groundingUpperBoundAngle;

            float distanceRatio = Mathf.Lerp(groundingMinPercentFromcenter, groundingMaxPercentFromCenter, angleRatio);

            Vector3 p = Math3d.ProjectPointOnPlane(characterController.up, characterController.transform.position, point);

            float distanceFromCenter = Vector3.Distance(p, characterController.transform.position);

            return distanceFromCenter <= distanceRatio * characterController.sphereRadius;
        }

        public Vector3 PrimaryNormal()
        {
            return primaryGround.Normal;
        }

        public float Distance()
        {
            return primaryGround.Distance;
        }

        public void DebugGround(bool primary, bool near, bool far, bool flush, bool step)
        {
            if (primary && primaryGround != null)
            {
                DebugDraw.DrawVector(primaryGround.Point, primaryGround.Normal, 2.0f, 1.0f, Color.yellow, 0, false);
            }

            if (near && nearGround != null)
            {
                DebugDraw.DrawVector(nearGround.Point, nearGround.Normal, 2.0f, 1.0f, Color.blue, 0, false);
            }

            if (far && farGround != null)
            {
                DebugDraw.DrawVector(farGround.Point, farGround.Normal, 2.0f, 1.0f, Color.red, 0, false);
            }

            if (flush && flushGround != null)
            {
                DebugDraw.DrawVector(flushGround.Point, flushGround.Normal, 2.0f, 1.0f, Color.cyan, 0, false);
            }

            if (step && stepGround != null)
            {
                DebugDraw.DrawVector(stepGround.Point, stepGround.Normal, 2.0f, 1.0f, Color.green, 0, false);
            }
        }

        /// <summary>
        /// Provides raycast data based on where a SphereCast would contact the specified normal
        /// Raycasting downwards from a point along the characterController's bottom sphere, based on the provided
        /// normal
        /// </summary>
        /// <param name="groundNormal">Normal of a triangle assumed to be directly below the characterController</param>
        /// <param name="rayHitInfo">Simulated SphereCast data</param>
        /// <returns>True if the raycast is successful</returns>
        private bool SimulateSphereCast(Vector3 groundNormal, out RaycastHit rayHitInfo)
        {
            float groundAngle = Vector3.Angle(groundNormal, characterController.up) * Mathf.Deg2Rad;

            Vector3 secondaryOrigin = characterController.transform.position + characterController.up * Tolerance;

            if (!Mathf.Approximately(groundAngle, 0))
            {
                float horizontal = Mathf.Sin(groundAngle) * characterController.sphereRadius;
                float vertical = (1.0f - Mathf.Cos(groundAngle)) * characterController.sphereRadius;

                // Retrieve a vector pointing up the slope
                Vector3 r2 = Vector3.Cross(groundNormal, characterController.down);
                Vector3 v2 = -Vector3.Cross(r2, groundNormal);

                secondaryOrigin += Math3d.ProjectVectorOnPlane(characterController.up, v2).normalized * horizontal + characterController.up * vertical;
            }

            if (Physics.Raycast(secondaryOrigin, characterController.down, out rayHitInfo, Mathf.Infinity, walkableLayer, triggerInteraction))
            {
                // Remove the tolerance from the distance travelled
                rayHitInfo.distance -= Tolerance + TinyTolerance;

                return true;
            }
            else
            {
                return false;
            }
        }
    }
}
