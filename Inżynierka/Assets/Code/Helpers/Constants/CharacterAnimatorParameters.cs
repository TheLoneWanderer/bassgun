﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Assets.Code.Helpers.Constants
{
    public static class CharacterConsts
    {
        public const float ANIMATION_SPEED_MULTIPLIER = 1.0f;

        public const float PUNCH_DURATION = 0.65f;
        public const float KICK_DURATION = 0.9f;

        public const float TWOHANDSWORD_SWING_MOVE_LOCK_DURATION = 0.80f;
        public const float TWOHANDSWORD_SWING_ACTION_LOCK_DURATION = 0.70f;
        public const float TWOHANDSWORD_SWING_COLLIDER_ACTIVE_DURATION = 0.50f;

        public const float TWOHANDSWORD_UNSHEATH_DURATION = 0.85f;
        public const float TWOHANDSWORD_SHEATH_DURATION = 0.85f;

        public const float ROLL_DURATION = 0.35f;
        public const float TURN_DURATION = 0.55f;
        public const float GET_HIT_DELAY = 0.1f;
        public const float GET_HIT_DURATION = 0.4f;
    }

    public static class CharacterAnimatorParameters
    {
            public enum UnarmedPunchAction
            {
                UnarmedPunchL1 = 1,
                UnarmedPunchL2 = 2,
                UnarmedPunchL3 = 3,

                UnarmedPunchR1 = 4,
                UnarmedPunchR2 = 5,
                UnarmedPunchR3 = 6
            }

            public enum SwordAttackAction
            {
                SwordSliceL1 = 1,
                SwordSliceL2 = 2,
                SwordSliceL3 = 3,

                SwordSliceR1 = 4,
                SwordSliceR2 = 5,
                SwordSliceR3 = 6
            }

            public enum KickAction
            {
                KickL1 = 1,
                KickR1 = 3,
            }

            public enum SwitchWeaponAction
            {
                Sheath = 0,
                Unsheath = 1
            }

            public enum GetHitAction
            {
                GetHitF1 = 1,
                GetHitF2 = 2,
                GetHitB1 = 3,
                GetHitL1 = 4,
                GetHitR1 = 5,
            }

            public enum RollAction
            {
                RollForward = 1,
                RollRight = 2,
                RollBackward = 3,
                RollLeft = 4
            }

            public enum JumpingState
            {
                Ground = 0,
                Jump = 1,
                DoubleJump = 2,
                Fall = 3
            }

            public static class AnimatorTriggers
            {
                public const string Idle = "IdleTrigger";
                public const string Jump = "JumpTrigger";
                public const string Roll = "RollTrigger";

                public const string TurnLeft = "TurnLeftTrigger";
                public const string TurnRight = "TurnRightTrigger";

                public const string Attack = "AttackTrigger";
                public const string AttackKick = "AttackKickTrigger";

                public const string WeaponSwitch = "WeaponSwitchTrigger";

                public const string GetHit = "GetHitTrigger";
                public const string Death1 = "Death1Trigger";
                public const string Revive1 = "Revive1Trigger";
            }
    } 
}
