﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Assets.Code.Helpers.Constants
{
    public static class SkeletonConsts
    {
        public const float WEAPON_SWING_LOCK_DURATION = 2.2f;
        public const float GET_HIT_LOCK_DURATION = 1f;
    }

    public static class SkeletonAnimatorParameters
    {
        public static class ParameterKeys
        {
            public const string Action = "Action";
            public const string VelocityX = "Velocity X";
            public const string VelocityZ = "Velocity Z";
        }

        public enum UnarmedAttackAction
        {
            UnarmedAttack1 = 0
        }
        
        public enum GetHitAction
        {
            GetHit1 = 0
        }

        public enum FallOverAction
        {
            FallOver1 = 0
        }

        public static class AnimatorTriggers
        {
            public const string Attack = "AttackTrigger";
            public const string GetHit = "GetHitTrigger";
            public const string FallOver = "FallOverTrigger";
            public const string GetUp = "GetUpTrigger";
        }
    }
}
