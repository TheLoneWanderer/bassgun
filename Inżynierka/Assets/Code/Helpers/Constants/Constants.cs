﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;

namespace Assets.Code.Helpers.Constants
{
    public static class Consts
    {
        // Tags
        public const string PlayerUniqueTag = "Player";
        public const string HittableEnemyTag = "HittableEnemy";

        // Used for finding nested components inside Player/Enemy
        public const string UIComponentName = "UI";
        public const string HealthBarComponentName = "HealthBar";
        public const string AttacksLeftBarComponentName = "AttacksBar";

        // Global objects
        public const string ObjectPoolerName = "ObjectPooler";

        public enum Side
        {
            Left = 1,
            Right = 2
        }

        public enum Direction
        {
            Forward = 1,
            Right = 2,
            Backward = 3,
            Left = 4
        }
    }
}
