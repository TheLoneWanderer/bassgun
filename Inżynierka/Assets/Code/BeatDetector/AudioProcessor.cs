﻿/*
 * Copyright (c) 2015 Allan Pichardo
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *  http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

using System;
using System.Linq;
using UnityEngine;

[RequireComponent(typeof(AudioSource))]
public class AudioProcessor : MonoBehaviour
{
	public AudioSource audioSource;

	private int samplingRate;
	private int nyquistFrequency;

	// granularity FFT wyniesie: nyquistFrequency (samplingRateUtworu/2) podzielona prez podaną niżej
	// czyli w przypadku 1024 -> od ok. 23.47 do nyquistFrequency
	// czyli po prostu sufitem jest zawsze nyquistFrequency, a poniżej podaje się tak jakby ilość grup (a dokładnie "binów") zwróconych częstotliwości
	public int fftSpectrumSize = 1024;
	private float[] fftSpectrum;


	// tablica binów zwrócona przez GetSpectrumData będzie później podzielona na większe grupy częstotliwości i miała wyliczoną średnią, to jest liczba grup
	const int frequencyBandsAmount = 12;
	float[] subSpectrumsAverageAmplitudes;
	float[] previousSpectrumAveragesInDb;


	int colmax = 120;
	float[] scorefun;

	// trade-off constant between tempo deviation penalty and onset strength
	private float alph;
	public float gThresh_AkaAlphSensitivityModifier = 0.1f;

	private AutoCorrelator autoCo;


	// index for circular buffer
	int circularBufferPerFrameIndex = 0;

	// counter to suppress double-beats
	int framesSinceLastBeat = 0;


	[Header("Events")]
	public OnBeatEventHandler onBeat;

	private void initArrays()
	{
		fftSpectrum = new float[fftSpectrumSize];

		subSpectrumsAverageAmplitudes = new float[frequencyBandsAmount];
		previousSpectrumAveragesInDb = Enumerable.Repeat(100.0f, frequencyBandsAmount).ToArray();

		scorefun = new float[colmax];
	}

	// Use this for initialization
	void Start()
	{
		initArrays();

		audioSource = GetComponent<AudioSource>();
		samplingRate = audioSource.clip.frequency;
		nyquistFrequency = samplingRate / 2;
		alph = 100 * gThresh_AkaAlphSensitivityModifier;

		float framePeriod = (float)fftSpectrumSize / (float)samplingRate;
		autoCo = new AutoCorrelator(framePeriod, getFrequencyDomainBinWidth());
	}

	// Update is called once per frame
	void Update()
	{
		if (audioSource.isPlaying)
		{
			audioSource.GetSpectrumData(fftSpectrum, 0, FFTWindow.BlackmanHarris);
			computeAverages(fftSpectrum);

			/* calculate the value of the onset function in this frame */
			// onset function is the sum of dB increments
			float dbIncreasesCurrentFrame = 0;
			for (int i = 0; i < frequencyBandsAmount; i++)
			{
				// dB value of this band
				float subSpectrumAverageInDb = (float)Math.Max(-100.0f, 20.0f * (float)Math.Log10(subSpectrumsAverageAmplitudes[i]) + 160);
				subSpectrumAverageInDb *= 0.025f;

				// dB increment since last frame
				float dbInc = subSpectrumAverageInDb - previousSpectrumAveragesInDb[i];

				// record this frame to use next time around
				previousSpectrumAveragesInDb[i] = subSpectrumAverageInDb;

				dbIncreasesCurrentFrame += dbInc;
			}

			/* update autocorrelator and find peak lag = current tempo */
			autoCo.AddValue(dbIncreasesCurrentFrame);

			// record largest value in (weighted) autocorrelation as it will be the tempo
			var correlateMaxValueIndex = autoCo.GetLargestValueIndex();

			/* calculate DP-ish function to update the best-score function */
			float scoreMax = -999999;
			
			// consider all possible preceding beat times
			for (int i = correlateMaxValueIndex / 2; i < Math.Min(colmax, 2 * correlateMaxValueIndex); ++i)
			{
				// czemu używa aktualnego indeksu (CB) w połączeniu z (aktualnym - tenZAutoCorrelation)?
				// kluczowy hint: tenZAutoCorrelation jest indeksem największego wzrostu Db w klatce w ostatnich iluś (delaysMaxAmount z AutoCorrelatora)
				//
				// jaki jest zakres offsetu offsetIndex i czemu akurat taki?
				// on nie szuka w całej liście największego, tylko jakby dookoła
				// to nie ma związku jakoś z wykrywaniem?
				//
				//
				//
				// na końcu traktuje wykrycie jako index największego == circular? wtf
				var offsetIndex = (circularBufferPerFrameIndex - i + colmax) % colmax;

				// objective function - this beat's cost + score to last beat + transition penalty
				float score = dbIncreasesCurrentFrame + scorefun[offsetIndex] - alph * (float)Math.Pow(Math.Log((float)i / (float)correlateMaxValueIndex), 2);
				// keep track of the best-scoring predecesor
				if (score > scoreMax)
				{
					scoreMax = score;
				}
			}

			scorefun[circularBufferPerFrameIndex] = scoreMax;
			// keep the smallest value in the score fn window as zero, by subtracing the min val
			float scoreMin = scorefun[0];
			for (int i = 0; i < colmax; ++i)
				if (scoreMin > scorefun[i])
					scoreMin = scorefun[i];

			for (int i = 0; i < colmax; ++i)
				scorefun[i] -= scoreMin;

			/* find the largest value in the score fn window, to decide if we emit a blip */
			scoreMax = scorefun[0];
			int scoreMaxIndex = 0;
			for (int i = 0; i < colmax; ++i)
			{
				if (scorefun[i] > scoreMax)
				{
					scoreMax = scorefun[i];
					scoreMaxIndex = i;
				}
			}

			++framesSinceLastBeat;
			// if current value is largest in the array, probably means we're on a beat
			if (scoreMaxIndex == circularBufferPerFrameIndex)
			{
				// make sure the most recent beat wasn't too recently
				if (framesSinceLastBeat > correlateMaxValueIndex / 4)
				{
					onBeat.Invoke();

					// reset counter of frames since last beat
					framesSinceLastBeat = 0;
				}
			}

			/* update column index (for ring buffer) */
			if (++circularBufferPerFrameIndex == colmax)
				circularBufferPerFrameIndex = 0;
		}
	}

	private void computeAverages(float[] fftSpectrumData)
	{
		// bierze tylko 512 z 1024, bo FFT ma to do siebie, że druga połowa danych się nie przydaje (jest jakoś "odbiciem lustrzanym", cokolwiek to znaczy)
		// od 0 do 512 są amplitudy z zakresu 0 do samplingRate/2 - w binach po 43 Hz (44100 / 1024)
		for (int i = 0; i < frequencyBandsAmount; i++)
		{
			float freqGroupAvg = 0;

			int lowBoundFreq;
			if (i == 0)
				lowBoundFreq = 0;
			else
				lowBoundFreq = (int)(nyquistFrequency / Math.Pow(2, frequencyBandsAmount - i));

			int highBoundFreq = (int)(nyquistFrequency / Math.Pow(2, (frequencyBandsAmount - 1) - i));

			int lowBoundIndex = frequencyToIndex(lowBoundFreq);
			int highBoundIndex = frequencyToIndex(highBoundFreq);

			for (int j = lowBoundIndex; j <= highBoundIndex; j++)
			{
				freqGroupAvg += fftSpectrumData[j];
			}

			freqGroupAvg /= (highBoundIndex - lowBoundIndex + 1);
			subSpectrumsAverageAmplitudes[i] = freqGroupAvg;
		}
	}

	private int frequencyToIndex(int freq)
	{
		// special case: freq is lower than the bin width of spectrum[0]
		if (freq < getFrequencyDomainBinWidth() / 2)
			return 0;

		// special case: freq is within the bin width of spectrum[spectrumSize/2]
		if (freq > nyquistFrequency - getFrequencyDomainBinWidth() / 2)
			return (fftSpectrumSize / 2);

		// all other cases
		float fraction = (float)freq / (float)samplingRate;
		int index = (int)Math.Round(fraction * fftSpectrumSize);

		return index;
	}

	private float getFrequencyDomainBinWidth()
	{
		var binWidth = samplingRate / fftSpectrumSize;
		return binWidth;
	}

	[System.Serializable]
	public class OnBeatEventHandler : UnityEngine.Events.UnityEvent
	{
	}


	// class to compute an array of online autocorrelators
	// auto means self in this case, not automatic (self-correlation in time due to delays from real-time computing)
	private class AutoCorrelator
	{
		// largest lag to track (in frames)
		private int delaysMaxAmount = 100;
		private int delaysRBIndex = 0;

		private float[] delaysRingBuffer;
		private float[] outputs;
		private float[] weights;

		public AutoCorrelator(float framePeriod, float bandwidth)
		{
			delaysRingBuffer = new float[delaysMaxAmount];
			outputs = new float[delaysMaxAmount];
			weights = new float[delaysMaxAmount];

			float bpmToWeigh;
			const float midBpm = 120f;

			// calculate a log-lag gaussian weighting function, to prefer tempi around 120 bpm
			for (int i = 0; i < delaysMaxAmount; ++i)
			{
				bpmToWeigh = 60.0f / (framePeriod * i);

				// weighting is Gaussian on log-BPM axis, centered at wMidBpm (~22nd element), SD = woctavewidth octaves
				weights[i] = (float)Math.Exp(-0.5f * Math.Pow(Math.Log(bpmToWeigh / midBpm) / Math.Log(2.0f) / bandwidth, 2.0f));
			}
		}

		// newValue <- suma różnic decybeli między poprzednim a aktualnym spectrum
		public void AddValue(float newValue)
		{
			delaysRingBuffer[delaysRBIndex] = newValue;

			// smoothing constant for running average
			const float decay = 0.003f;

			// update running autocorrelator values
			for (int index = 0; index < delaysMaxAmount; ++index)
			{
				int reverseIndex = (delaysRBIndex - index + delaysMaxAmount) % delaysMaxAmount;
				outputs[index] += (delaysRingBuffer[delaysRBIndex] * delaysRingBuffer[reverseIndex] - outputs[index]) * decay;
			}

			if (++delaysRBIndex == delaysMaxAmount)
				delaysRBIndex = 0;
		}

		// zwraca -> ile klatek temu była klatka z największym wzrostem energii?
		// outputs ma w sobie tak jakby wzrosty energii, bo przemnaża aktualną wartość przez każdą po kolei do tyłu ze sobą włącznie
		// sama przez siebie wyjdzie zazwyczaj dużo i jest potencjalnie największa, jak inna będzie większa to znaczy że tam był największy wzrost, a jak mniejsza to ignorujemy
		// AutoCorrelation bierze pod uwagę wartości o 16ms * 100 => 166ms do tyłu od aktualnej
		public int GetLargestValueIndex()
		{
			int maxValueIndex = 0;
			float currentMaxValue = 0.0f;

			for (int i = 0; i < delaysMaxAmount; ++i)
			{
				// ten Sqrt nie jest dlatego, że w AddValue jest mnożenie przez samą siebie? (w przypadku najnowszej wartości)
				float correlateValue = (float)Math.Sqrt(Correlate(i));
				if (correlateValue > currentMaxValue)
				{
					currentMaxValue = correlateValue;
					maxValueIndex = i;
				}
			}

			return maxValueIndex;
		}

		// read back the current autocorrelator value at a particular lag
		private float Correlate(int delayIndex)
		{
			return outputs[delayIndex] * weights[delayIndex];
		}
	}

	public void DebugDisplayGetSpectrumResult(int spectrumSize = 64, int spectrumSize2 = 512, int spectrumSize3 = 2048)
	{
		int testSpectrumSize = spectrumSize;
		int testSpectrumSize2 = spectrumSize2;
		int testSpectrumSize3 = spectrumSize3;

		// 0
		float[] testSpectrum = new float[testSpectrumSize];
		audioSource.GetSpectrumData(testSpectrum, 0, FFTWindow.BlackmanHarris);

		for (int i = 0; i < testSpectrum.Length / 2; i++)
		{
			var lineOffset = 0.002f;
			var secondLineOffset = i + lineOffset;
			var thirdLineOffset = secondLineOffset + lineOffset;

			var columnBottom = 0;
			var columnHeight = columnBottom + testSpectrum[i] * 100;

			Debug.DrawLine(new Vector3(i, columnBottom, 0), new Vector3(i, columnHeight, 0), Color.red, 1 / 60);
			Debug.DrawLine(new Vector3(i + secondLineOffset, columnBottom, 0), new Vector3(i + secondLineOffset, columnHeight, 0), Color.red, 1 / 60);
			Debug.DrawLine(new Vector3(i + thirdLineOffset, columnBottom, 0), new Vector3(i + thirdLineOffset, columnHeight, 0), Color.red, 1 / 60);
		}

		// 2
		float[] testSpectrum2 = new float[testSpectrumSize2];
		audioSource.GetSpectrumData(testSpectrum2, 0, FFTWindow.BlackmanHarris);

		for (int i = 0; i < testSpectrum2.Length / 2; i++)
		{
			var lineOffset = 0.002f;
			var secondLineOffset = i + lineOffset;
			var thirdLineOffset = secondLineOffset + lineOffset;

			var zAxisOffset = -20;

			var columnBottom = 0;
			var columnHeight = columnBottom + testSpectrum2[i] * 100;

			Debug.DrawLine(new Vector3(i, columnBottom, zAxisOffset), new Vector3(i, columnHeight, zAxisOffset), Color.blue, 1 / 60);
			Debug.DrawLine(new Vector3(i + secondLineOffset, columnBottom, zAxisOffset), new Vector3(i + secondLineOffset, columnHeight, zAxisOffset), Color.blue, 1 / 60);
			Debug.DrawLine(new Vector3(i + thirdLineOffset, columnBottom, zAxisOffset), new Vector3(i + thirdLineOffset, columnHeight, zAxisOffset), Color.blue, 1 / 60);
		}

		// 3
		float[] testSpectrum3 = new float[testSpectrumSize3];
		audioSource.GetSpectrumData(testSpectrum3, 0, FFTWindow.BlackmanHarris);

		for (int i = 0; i < testSpectrum3.Length / 2; i++)
		{
			var multipleLinesSideToSideOffset = 0.002f;
			var secondLineOffset = i + multipleLinesSideToSideOffset;
			var thirdLineOffset = secondLineOffset + multipleLinesSideToSideOffset;

			var zAxisOffset = -40;

			var columnBottom = 0;
			var columnHeight = columnBottom + testSpectrum3[i] * 100;

			Debug.DrawLine(new Vector3(i, columnBottom, zAxisOffset), new Vector3(i, columnHeight, zAxisOffset), Color.green, 1 / 60);
			Debug.DrawLine(new Vector3(i + secondLineOffset, columnBottom, zAxisOffset), new Vector3(i + secondLineOffset, columnHeight, zAxisOffset), Color.green, 1 / 60);
			Debug.DrawLine(new Vector3(i + thirdLineOffset, columnBottom, zAxisOffset), new Vector3(i + thirdLineOffset, columnHeight, zAxisOffset), Color.green, 1 / 60);
		}
	}

	public static void DebugDisplayArray(float[] arrayToDisplay, float zAxisOffset, Color color, float durationInSeconds, float sizeModifier = 1f)
	{
		for (int i = 0; i < arrayToDisplay.Length; i++)
		{
			Debug.DrawLine(new Vector3(i, 0, zAxisOffset), new Vector3(i, arrayToDisplay[i] * sizeModifier, zAxisOffset), color, durationInSeconds);
		}
	}
}
