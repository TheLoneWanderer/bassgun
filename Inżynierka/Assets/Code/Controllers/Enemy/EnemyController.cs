﻿using Assets.Code.Combat.SpawningObjects;
using Assets.Code.Combat.SpawningObjects.ObjectPooling;
using Assets.Code.Helpers.Constants;
using Assets.Code.Helpers.DTO;
using Assets.Code.ReceiveDamageStrategies;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEditorInternal;
using UnityEngine;
using UnityEngine.AI;
using UnityEngine.UI;

namespace Assets.Code.Controllers.Enemy
{
    public enum EnemySkeletonState
    {
        Idle,
        Following,
        Fight,
        GetHit,
        Dead
    }

    public class EnemyController : SuperStateMachine
    {
        private GameObject target;
        private Animator animator;
        private NavMeshAgent navMeshAgent;
        private Slider healthSlider;
        private Slider attacksLeftSlider;

        private Vector3 targetPosition;

        private bool canAttack;
        private float attackRange;
        private DamageCharacterEntry damageCharacterEntry;

        void Start()
        {
            target = GameObject.FindGameObjectWithTag(Consts.PlayerUniqueTag);
            animator = GetComponent<Animator>();
            navMeshAgent = GetComponent<NavMeshAgent>();

            var UiComponentsTransform = transform.Find(Consts.UIComponentName);
            healthSlider = UiComponentsTransform.Find(Consts.HealthBarComponentName).GetComponentInChildren<Slider>();
            attacksLeftSlider = UiComponentsTransform.Find(Consts.AttacksLeftBarComponentName).GetComponentInChildren<Slider>();

            currentState = EnemySkeletonState.Following;

            canAttack = true;
            attackRange = navMeshAgent.stoppingDistance;

            damageCharacterEntry = new DamageCharacterEntry();
            damageCharacterEntry.healthSlider = healthSlider;
        }

        void Update()
        {
            UpdateAnimatorVelocity();
            targetPosition = target.transform.position;

            if (Input.GetKeyDown(KeyCode.T))
            {
                Resurrect();
            }
        }

        void Idle_SuperUpdate()
        {
        }

        void Following_SuperUpdate()
        {
            navMeshAgent.SetDestination(targetPosition);

            if (IsInAttackRange(attackRange, targetPosition) && !IsAgentMoving(navMeshAgent))
            {
                currentState = EnemySkeletonState.Fight;
            }
        }

        void Fight_EnterState()
        {
            // Później uzupełniać slider z wykrytego bassu utworu (X uderzeń bassu to jeden atak)
            attacksLeftSlider.value = attacksLeftSlider.maxValue;
        }

        void Fight_SuperUpdate()
        {
            if (!IsInAttackRange(attackRange, targetPosition))
            {
                currentState = EnemySkeletonState.Following;
            }
            else
            {
                if (attacksLeftSlider.value > 0 && canAttack)
                {
                    Attack();
                }
            }
        }

        void GetHit_EnterState()
        {
            Debug.Log("GetHit_Enter");

            animator.SetInteger(SkeletonAnimatorParameters.ParameterKeys.Action, (int)SkeletonAnimatorParameters.GetHitAction.GetHit1);
            animator.SetTrigger(SkeletonAnimatorParameters.AnimatorTriggers.GetHit);

            DisableActions(SkeletonConsts.GET_HIT_LOCK_DURATION, SkeletonConsts.GET_HIT_LOCK_DURATION);

            if (healthSlider.value == 0)
            {
                currentState = EnemySkeletonState.Dead;
                return;
            }

            currentState = EnemySkeletonState.Fight;
        }

        void Dead_EnterState()
        {
            Die();
        }

        public void TakeDamage(IReceiveDamageStrategy receiveDamageStrategy)
        {
            receiveDamageStrategy.ReceiveDamage(damageCharacterEntry);
            currentState = EnemySkeletonState.GetHit;
        }

        void Die()
        {
            StartCoroutine("DieCoroutine");
        }

        IEnumerator DieCoroutine()
        {
            GetComponent<Collider>().enabled = false;
            transform.Find(Consts.UIComponentName).gameObject.SetActive(false);

            animator.SetInteger(SkeletonAnimatorParameters.ParameterKeys.Action, (int)SkeletonAnimatorParameters.FallOverAction.FallOver1);
            animator.SetTrigger(SkeletonAnimatorParameters.AnimatorTriggers.FallOver);

            yield return new WaitForSeconds(2);

            var objectPooler = GameObject.Find(Consts.ObjectPoolerName);
            var objectPoolData = GetComponent<PoolableObjectData>();

            if (objectPooler == null || objectPoolData == null)
            {
                Debug.LogError("Can't pull object back to the pool. Either the object pooler is not active or PoolableObjectData is not attached to this gameObject");

                yield break;
            }

            objectPooler.GetComponent<ObjectPooler>().PutBackObject(objectPoolData.GetPoolKey(), this.gameObject);
        }

        void Resurrect()
        {
            GetComponent<Collider>().enabled = true;
            transform.Find(Consts.UIComponentName).gameObject.SetActive(true);

            healthSlider.value = healthSlider.maxValue;

            animator.SetTrigger(SkeletonAnimatorParameters.AnimatorTriggers.GetUp);
            currentState = EnemySkeletonState.Following;
        }

        void Attack()
        {
            attacksLeftSlider.value--;

            transform.LookAt(targetPosition);
            animator.SetInteger(SkeletonAnimatorParameters.ParameterKeys.Action, (int)SkeletonAnimatorParameters.UnarmedAttackAction.UnarmedAttack1);
            animator.SetTrigger(SkeletonAnimatorParameters.AnimatorTriggers.Attack);

            DisableActions(SkeletonConsts.WEAPON_SWING_LOCK_DURATION, SkeletonConsts.WEAPON_SWING_LOCK_DURATION);
        }

        bool IsInAttackRange(float attackRange, Vector3 targetPos)
        {
            var distanceFromTarget = Vector3.Distance(transform.position, targetPos);
            if (distanceFromTarget <= attackRange)
            {
                return true;
            }

            return false;
        }

        bool IsAgentMoving(NavMeshAgent agent)
        {
            if (!agent.pathPending)
            {
                if (agent.remainingDistance <= agent.stoppingDistance)
                {
                    if (!agent.hasPath || agent.velocity.sqrMagnitude == 0f)
                    {
                        return false;
                    }
                }
            }

            return true;
        }

        /// <summary>
        /// Disable both movement and attacking for a specified period of time
        /// </summary>
        /// <param name="movementLockDuration"></param>
        /// <param name="attackingLockDuration"></param>
        void DisableActions(float movementLockDuration, float attackingLockDuration)
        {
            DisableMovement(movementLockDuration);
            DisableAttacking(attackingLockDuration);
        }

        void DisableMovement(float lockDuration)
        {
            StopCoroutine("DisableMovementCoroutine");
            StartCoroutine("DisableMovementCoroutine", lockDuration);
        }

        IEnumerator DisableMovementCoroutine(float lockDuration)
        {
            navMeshAgent.enabled = false;
            yield return new WaitForSeconds(lockDuration);
            navMeshAgent.enabled = true;
        }

        void DisableAttacking(float lockDuration)
        {
            StopCoroutine("DisableAttackingCoroutine");
            StartCoroutine("DisableAttackingCoroutine", lockDuration);
        }

        IEnumerator DisableAttackingCoroutine(float lockDuration)
        {
            canAttack = false;
            yield return new WaitForSeconds(lockDuration);
            canAttack = true;
        }

        void UpdateAnimatorVelocity()
        {
            var velocityXKey = SkeletonAnimatorParameters.ParameterKeys.VelocityX;
            var velocityZKey = SkeletonAnimatorParameters.ParameterKeys.VelocityZ;

            var velocityX = navMeshAgent.velocity.x;
            var velocityZ = navMeshAgent.velocity.z;

            animator.SetFloat(velocityXKey, velocityX);
            animator.SetFloat(velocityZKey, velocityZ);
        }
    }
}