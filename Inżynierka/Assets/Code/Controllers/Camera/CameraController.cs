﻿using UnityEngine;
using System.Collections;
using Assets.Code.Helpers.Constants;

public class CameraController : MonoBehaviour
{
	GameObject cameraTarget;

	float rotateInput;
	public float rotateSpeed;

	public float offsetHeight;
	public float offsetDistance;
	public float smoothing;

	bool following = true;
	Vector3 offset;
	Vector3 cameraLastPosition;

	void Start()
	{
		cameraTarget = GameObject.FindGameObjectWithTag(Consts.PlayerUniqueTag);
		offset = new Vector3(0, offsetHeight, -offsetDistance);
		cameraLastPosition = new Vector3(cameraTarget.transform.position.x, cameraTarget.transform.position.y + offset.y, cameraTarget.transform.position.z + offset.z);
	}

	void Update()
	{
		if(Input.GetKeyDown(KeyCode.F))
		{
			following = !following;
		}

		rotateInput = 0;
		if (Input.GetKey(KeyCode.Q))
		{
			rotateInput = -1;
		} 
		else if(Input.GetKey(KeyCode.E))
		{
			rotateInput = 1;
		}

		if(following)
		{
			var rotationAngle = rotateInput * rotateSpeed;
			Quaternion aroundRotation = Quaternion.AngleAxis(rotationAngle, Vector3.up);

			offset = aroundRotation * offset;

			var newXPosition = Mathf.Lerp(cameraLastPosition.x, cameraTarget.transform.position.x + offset.x, smoothing * Time.deltaTime);
			var newYPosition = Mathf.Lerp(cameraLastPosition.y, cameraTarget.transform.position.y + offset.y, smoothing * Time.deltaTime);
			var newZPosition = Mathf.Lerp(cameraLastPosition.z, cameraTarget.transform.position.z + offset.z, smoothing * Time.deltaTime);
			Vector3 newCameraPosition = new Vector3(newXPosition, newYPosition, newZPosition);

			transform.position = newCameraPosition;
		}
		else
		{
			transform.position = cameraLastPosition;
		}

		transform.LookAt(cameraTarget.transform.position);
	}

	void LateUpdate()
	{
		cameraLastPosition = transform.position;
	}
}