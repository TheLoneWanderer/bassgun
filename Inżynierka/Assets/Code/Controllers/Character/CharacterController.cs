﻿using Assets.Code.Helpers.Constants;
using System.Collections;
using UnityEngine;
using static Assets.Code.Helpers.Constants.Consts;
using static Assets.Code.Helpers.Constants.CharacterConsts;

namespace Assets.Code.Controllers.Character
{

	[RequireComponent(typeof(CharacterMovementController))]
	[RequireComponent(typeof(CharacterWeaponController))]
	[RequireComponent(typeof(CharacterInputController))]
	public class CharacterController : MonoBehaviour
	{
		[HideInInspector] public CharacterMovementController rpgCharacterMovementController;
		[HideInInspector] public CharacterWeaponController rpgCharacterWeaponController;
		[HideInInspector] public CharacterInputController rpgCharacterInputController;
		[HideInInspector] public Animator animator;
		[HideInInspector] public IKHandsFREE ikHands;

		public Weapon weapon = Weapon.UNARMED;
		public GameObject target;

		[HideInInspector] public bool isDead = false;
		[HideInInspector] public bool isStrafing = false;
		[HideInInspector] public bool canAction = true;

		void Awake()
		{
			rpgCharacterMovementController = GetComponent<CharacterMovementController>();
			rpgCharacterWeaponController = GetComponent<CharacterWeaponController>();
			rpgCharacterInputController = GetComponent<CharacterInputController>();
			animator = GetComponentInChildren<Animator>();
			ikHands = GetComponent<IKHandsFREE>();

			weapon = Weapon.UNARMED;
			animator.SetInteger("CurrentWeapon", (int)Weapon.UNARMED);
		}

		void Start()
		{
			rpgCharacterMovementController.SwitchCollisionOn();
			UpdateAnimationSpeed();
		}

		void Update()
		{
			if (rpgCharacterMovementController.MaintainingGround())
			{
				if (canAction)
				{
					Strafing();
					Rolling();

					if (rpgCharacterInputController.inputAttackL)
					{
						Attack(Side.Left);
					}
					else if (rpgCharacterInputController.inputAttackR)
					{
						Attack(Side.Right);
					}

					if (rpgCharacterInputController.inputSwitchUpDown != 0 && rpgCharacterWeaponController.isSwitchingFinished)
					{
						if (weapon == Weapon.UNARMED)
						{
							rpgCharacterWeaponController.SwitchWeaponTwoHand(Weapon.TWOHANDSWORD);
						}
						else
						{
							rpgCharacterWeaponController.SwitchWeaponTwoHand(Weapon.UNARMED);
						}
					}
				}
			}
			//Slow time toggle.
			if (Input.GetKeyDown(KeyCode.T))
			{
				if (Time.timeScale != 1)
				{
					Time.timeScale = 1;
				}
				else
				{
					Time.timeScale = 0.25f;
				}
			}
			//Pause toggle.
			if (Input.GetKeyDown(KeyCode.P))
			{
				if (Time.timeScale != 1)
				{
					Time.timeScale = 1;
				}
				else
				{
					Time.timeScale = 0f;
				}
			}

			animator.SetBool("Strafing", rpgCharacterInputController.inputStrafe);
		}

		void UpdateAnimationSpeed()
		{
			animator.SetFloat("AnimationSpeedMultiplier", ANIMATION_SPEED_MULTIPLIER);
		}

		public void Turn(Direction direction)
		{
			if (direction == Direction.Left)
			{
				animator.SetTrigger(CharacterAnimatorParameters.AnimatorTriggers.TurnLeft);
			}
			else if (direction == Direction.Right)
			{
				animator.SetTrigger(CharacterAnimatorParameters.AnimatorTriggers.TurnRight);
			}

			Lock(TURN_DURATION, TURN_DURATION, 0);
		}

		public void Attack(Side whichSide)
		{
			if (rpgCharacterMovementController.isMoving)
			{
				return;
			}

			int attackNumber = 0;
			if (weapon == Weapon.UNARMED)
			{
				if (whichSide == Side.Left)
				{
					int leftAttacksAmount = 3;
					attackNumber = Random.Range(1, leftAttacksAmount + 1);
				}
				else if (whichSide == Side.Right)
				{
					int rightAttacksAmount = 3;
					attackNumber = Random.Range(4, rightAttacksAmount + 4);
				}

				Lock(PUNCH_DURATION, PUNCH_DURATION, 0);
			}
			else if (weapon == Weapon.TWOHANDSWORD)
			{
				if (whichSide == Side.Left)
				{
					int leftAttacksAmount = 3;
					attackNumber = Random.Range(1, leftAttacksAmount + 1);
				}
				else if (whichSide == Side.Right)
				{
					int rightAttacksAmount = 3;
					attackNumber = Random.Range(4, rightAttacksAmount + 4);
				}

				BlinkMeleeWeaponCollider(TWOHANDSWORD_SWING_COLLIDER_ACTIVE_DURATION);
				Lock(TWOHANDSWORD_SWING_MOVE_LOCK_DURATION, TWOHANDSWORD_SWING_ACTION_LOCK_DURATION, 0);
			}

			animator.SetInteger("Action", attackNumber);
			animator.SetTrigger(CharacterAnimatorParameters.AnimatorTriggers.Attack);
		}

		public void AttackKick(Side whichLeg)
		{
			if (whichLeg == Side.Left)
			{
				animator.SetInteger("Action", (int)CharacterAnimatorParameters.KickAction.KickL1);
			}
			else if (whichLeg == Side.Right)
			{
				animator.SetInteger("Action", (int)CharacterAnimatorParameters.KickAction.KickR1);
			}

			animator.SetTrigger(CharacterAnimatorParameters.AnimatorTriggers.AttackKick);
			Lock(KICK_DURATION, KICK_DURATION, 0);
		}

		void Strafing()
		{
			var strafingInput = rpgCharacterInputController.inputStrafe;

			animator.SetBool("Strafing", strafingInput);
			isStrafing = strafingInput;
		}

		void Rolling()
		{
			if (!rpgCharacterMovementController.isRolling && rpgCharacterInputController.inputRoll)
			{
				rpgCharacterMovementController.DirectionalRoll();
			}
		}

		public void GetHit()
		{
			int possibleHitDirections = 4;
			int hitDirection = Random.Range(1, possibleHitDirections + 1);

			animator.SetInteger("Action", hitDirection);
			animator.SetTrigger(CharacterAnimatorParameters.AnimatorTriggers.GetHit);
			Lock(GET_HIT_DURATION, GET_HIT_DURATION, GET_HIT_DELAY);

			const int BACK_HIT = 1;
			const int FRONT_HIT = 2;
			const int RIGHT_HIT = 3;
			const int LEFT_HIT = 4;

			if (hitDirection == BACK_HIT)
			{
				StartCoroutine(rpgCharacterMovementController.Knockback(-transform.forward, 8, 4));
			}
			else if (hitDirection == FRONT_HIT)
			{
				StartCoroutine(rpgCharacterMovementController.Knockback(transform.forward, 8, 4));
			}
			else if (hitDirection == RIGHT_HIT)
			{
				StartCoroutine(rpgCharacterMovementController.Knockback(transform.right, 8, 4));
			}
			else if (hitDirection == LEFT_HIT)
			{
				StartCoroutine(rpgCharacterMovementController.Knockback(-transform.right, 8, 4));
			}
		}

		public void Death()
		{
			animator.SetTrigger(CharacterAnimatorParameters.AnimatorTriggers.Death1);
			Lock(0f, 0f, 0.1f);
			isDead = true;
		}

		public void Revive()
		{
			animator.SetTrigger(CharacterAnimatorParameters.AnimatorTriggers.Revive1);
			Lock(1f, 1f, 0f);
			isDead = false;
		}

		IEnumerator GetCurrentAnimationLength()
		{
			yield return new WaitForEndOfFrame();
			float f = animator.GetCurrentAnimatorClipInfo(0).Length;
			Debug.Log("Animation length: " + f);
		}

		public void BlinkMeleeWeaponCollider(float enableDuration)
		{
			StopCoroutine("BlinkMeleeWeaponColliderCoroutine");
			StartCoroutine("BlinkMeleeWeaponColliderCoroutine", enableDuration);
		}

		public IEnumerator BlinkMeleeWeaponColliderCoroutine(float enableDuration)
		{
			var weaponCollider = rpgCharacterWeaponController.equippedWeapon.GetComponentInChildren<Collider>();

			weaponCollider.enabled = true;
			yield return new WaitForSeconds(enableDuration);
			weaponCollider.enabled = false;
		}

		public void Lock(float lockMovementDuration, float lockActionDuration, float delayTime)
		{
			StopCoroutine("LockPlayerCoroutine");
			StartCoroutine(LockPlayerCoroutine(lockMovementDuration, lockActionDuration, delayTime));
		}

		private IEnumerator LockPlayerCoroutine(float lockMovementDuration, float lockActionDuration, float delayTime)
		{
			if (delayTime > 0)
			{
				yield return new WaitForSeconds(delayTime);
			}

			rpgCharacterMovementController.LockMovement(lockMovementDuration);
			LockAction(lockActionDuration);
		}

		void LockAction(float lockDurationInSeconds)
		{
			StopCoroutine("LockActionCoroutine");
			StartCoroutine("LockActionCoroutine", lockDurationInSeconds);
		}

		IEnumerator LockActionCoroutine(float lockDurationInSeconds)
		{
			canAction = false;

			if (lockDurationInSeconds > 0)
			{
				yield return new WaitForSeconds(lockDurationInSeconds);
			}

			canAction = true;
		}

		public void SetWeaponType(Weapon weaponNumber)
		{
			weapon = weaponNumber;
		}
	}
}