﻿using System.Collections;
using UnityEngine;
using static Assets.Code.Helpers.Constants.CharacterAnimatorParameters;
using static Assets.Code.Helpers.Constants.CharacterConsts;
using Assets.Code.Helpers.Constants;

namespace Assets.Code.Controllers.Character
{

	public enum Weapon
	{
		UNARMED = 0,
		TWOHANDSWORD = 1
	}

	public class CharacterWeaponController : MonoBehaviour
	{
		CharacterController rpgCharacterController;
		private Animator animator;

		//Weapon Parameters.
		[HideInInspector] public bool isSwitchingFinished = true;
		[HideInInspector] public bool isWeaponSwitching = false;

		public GameObject equippedWeapon;

		void Awake()
		{
			rpgCharacterController = GetComponent<CharacterController>();
			animator = GetComponentInChildren<Animator>();

			StartCoroutine(SetWeaponVisibility(false));
			SetWeaponType(Weapon.UNARMED);		
		}

		/// <summary>
		/// Controller weapon switching.
		/// </summary>
		public void SwitchWeaponTwoHand(Weapon nextWeapon)
		{
			SetWeaponSwitchingState(true);
			SwitchWeapon(nextWeapon);
			SetWeaponSwitchingState(false);
		}

		public void SwitchWeapon(Weapon nextWeapon)
		{
			var currentWeapon = (Weapon)animator.GetInteger("CurrentWeapon");
			if(IsUnarmed(currentWeapon))
			{
				StartCoroutine(UnSheathWeapon(nextWeapon));
			}
			else
			{
				StartCoroutine(SheathWeapon());

				if(nextWeapon != Weapon.UNARMED)
				{
					StartCoroutine(UnSheathWeapon(nextWeapon));
				}
			}
		}

		public IEnumerator UnSheathWeapon(Weapon nextWeaponType)
		{
			rpgCharacterController.Lock(0f, TWOHANDSWORD_UNSHEATH_DURATION, 0f);

			SetWeaponType(nextWeaponType);
			animator.SetInteger("Action", (int)SwitchWeaponAction.Unsheath);
			animator.SetTrigger(CharacterAnimatorParameters.AnimatorTriggers.WeaponSwitch);

			yield return new WaitForSeconds(0.5f);

			StartCoroutine(SetWeaponVisibility(true));

			if (rpgCharacterController.ikHands != null){
				rpgCharacterController.ikHands.BlendIK(true, 0.5f, 1, Weapon.TWOHANDSWORD);
			}
		}

		public IEnumerator SheathWeapon()
		{
			rpgCharacterController.Lock(0f, TWOHANDSWORD_SHEATH_DURATION, 0f);

			animator.SetInteger("Action", (int)SwitchWeaponAction.Sheath);
			animator.SetTrigger(CharacterAnimatorParameters.AnimatorTriggers.WeaponSwitch);

			yield return new WaitForSeconds(0.5f);

			StartCoroutine(SetWeaponVisibility(false));
			SetWeaponType(Weapon.UNARMED);

			if (rpgCharacterController.ikHands != null){
				rpgCharacterController.ikHands.BlendIK(false, 0f, 0.2f, Weapon.TWOHANDSWORD);
			}
		}

		
		/// <summary>
		/// Sets the animator state.
		/// </summary>
		/// <param name="weapon">Weapon.</param>
		void SetWeaponType(Weapon weapon)
		{
			animator.SetInteger("CurrentWeapon", (int)weapon);
			rpgCharacterController.SetWeaponType(weapon);
		}

		//For Animation Event.
		public void SetWeaponSwitchingState(bool isSwitching)
		{
			isSwitchingFinished = !isSwitching;
			isWeaponSwitching = isSwitching;	
		}

		public IEnumerator SetWeaponVisibility(bool makeVisible)
		{
			while (isWeaponSwitching){
				yield return null;
			}

			equippedWeapon.SetActive(makeVisible);

			yield return null;
		}

		public bool IsUnarmed(Weapon weaponType)
		{
			return weaponType == Weapon.UNARMED;
		}

		public bool Is2HandedWeapon(Weapon weaponType)
		{
			return weaponType == Weapon.TWOHANDSWORD;
		}
	}
}