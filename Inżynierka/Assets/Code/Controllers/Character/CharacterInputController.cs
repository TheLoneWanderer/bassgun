﻿using UnityEngine;
using System.Collections;

namespace Assets.Code.Controllers.Character{
	
	public class CharacterInputController : MonoBehaviour{

		//Inputs.
		[HideInInspector] public float inputHorizontal = 0;
		[HideInInspector] public float inputVertical = 0;
		[HideInInspector] public bool inputJump;
		[HideInInspector] public bool inputStrafe;

		[HideInInspector] public bool inputAttackL;
		[HideInInspector] public bool inputAttackR;
		[HideInInspector] public float inputSwitchUpDown;
		[HideInInspector] public float inputAimVertical = 0;
		[HideInInspector] public float inputAimHorizontal = 0;
		[HideInInspector] public bool inputRoll;

		//Variables
		[HideInInspector] public bool allowedInput = true;
		[HideInInspector] public Vector3 relativeMoveInput;
		[HideInInspector] public Vector2 aimInput;

		/// <summary>
		/// Input abstraction for easier asset updates using outside control schemes.
		/// </summary>
		void Inputs(){
			inputHorizontal = Input.GetAxisRaw("Horizontal");
			inputVertical = Input.GetAxisRaw("Vertical");
			inputJump = Input.GetButtonDown("Jump");
			inputStrafe = Input.GetKey(KeyCode.LeftShift);


			inputAttackL = Input.GetMouseButtonDown(0);
			inputAttackR = Input.GetMouseButtonDown(1);
			inputSwitchUpDown = Input.GetAxisRaw("Mouse ScrollWheel");

			inputRoll = Input.GetButtonDown("Roll");
		}

		void Awake(){
			allowedInput = true;
		}

		void Update(){
			Inputs();
			relativeMoveInput = CameraRelativeMovement(inputHorizontal, inputVertical);
		}

		/// <summary>
		/// Movement based off camera facing.
		/// </summary>
		Vector3 CameraRelativeMovement(float inputHorizontalAxis, float inputForwardAxis)
		{
			// Forward vector relative to the camera along the x-z plane
			Vector3 cameraForward = Camera.main.transform.TransformDirection(Vector3.forward);
			cameraForward.y = 0;
			cameraForward = cameraForward.normalized;

			// Right vector relative to the camera always orthogonal to the forward vector.
			Vector3 cameraRight = new Vector3(cameraForward.z, 0, -cameraForward.x);

			// Forward and horizontal vectors combined as the direction
			Vector3 relativeDirection = (inputHorizontalAxis * cameraRight) + (inputForwardAxis * cameraForward);

			// Reduce input for diagonal movement.
			if(relativeDirection.magnitude > 1){
				relativeDirection.Normalize();
			}
			return relativeDirection;
		}

		public bool HasAnyInput(){
			if(allowedInput && relativeMoveInput != Vector3.zero && aimInput != Vector2.zero && inputJump != false){
				return true;
			}
			else{
				return false;
			}
		}
		
		public bool HasMoveInput(){
			if(allowedInput && relativeMoveInput != Vector3.zero){
				return true;
			}
			else{
				return false;
			}
		}
		
		public bool HasAimInput(){
			if(allowedInput && (aimInput.x < -0.8f || aimInput.x > 0.8f) || (aimInput.y < -0.8f || aimInput.y > 0.8f)){
				return true;
			}
			else{
				return false;
			}
		}
	}
}